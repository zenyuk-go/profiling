manually add tracing regions in report
==
r := trace.StartRegion(ctx, "MY_REGION")
defer r.End()


generate output for analysis
==
go test -trace my_tracing.out ./...


analyse output in GUI
==
go tool trace my_tracing.out
// will open browser
